# Compose-Website
![Projet 1 - Compose-Website](https://img.shields.io/badge/Projet_1-Compose--Website-important?logo=googlescholar&logoColor=white)
![Ansible - Playbooks](https://img.shields.io/badge/Ansible-Playbooks-2ea44f?logo=Ansible)
![Docker - Compose](https://img.shields.io/badge/Docker-Compose-blue?logo=docker&logoColor=white)
![Hashicorp - Vagrant](https://img.shields.io/badge/Hashicorp-Vagrant-white?logo=vagrant&logoColor=blue)

## Sujet du projet:

>*Introduction :*
Le projet de déploiement continu de WordPress avec Jenkins vise à automatiser le processus de déploiement de sites WordPress en utilisant un pipeline d'intégration continue. WordPress est l'un des CMS les plus populaires au monde, mais le processus de déploiement peut souvent être fastidieux et sujet à des erreurs humaines. L'utilisation d'un pipeline d'intégration continue avec Jenkins permet de simplifier et d'automatiser ce processus, tout en améliorant la qualité et la fiabilité du déploiement.

>*Objectifs :*
>-Installer et configurer Jenkins pour un projet de déploiement continu de WordPress
>-Configurer un pipeline d'intégration continue pour le déploiement de sites WordPress à partir d'un dépôt Git
>-Automatiser le processus de déploiement de WordPress pour améliorer la qualité et la fiabilité.

>*Prérequis :*
>-Connaissance de base de WordPress, de Git et de Jenkins

>*Considérations techniques :*
>-Utiliser un dépôt Git pour stocker le code source de WordPress
>-Configurer Jenkins pour surveiller le dépôt Git et récupérer automatiquement les mises à jour du code source
>-Configurer un pipeline d'intégration continue pour le déploiement de sites WordPress
>-Configurer des notifications pour les erreurs de construction,de test ou de déploiement
>-Intégrer des outils de qualité du code tels que SonarQube pour surveiller la qualité de votre code source
>-Configurer des rapports de test détaillés pour suivre les résultats des tests au fil du temps

>*Consignes :*
>>1-Installation de Jenkins et des plugins nécessaires
>-Télécharger et installer Jenkins sur votre machine locale
>-Installer les plugins nécessaires pour le pipeline de déploiement continu : Git plugin, Pipeline plugin, Credentials plugin, etc.
2-Configuration de Git pour le dépôt de code source
-Créer un compte Git pour stocker votre code source
-Créer un dépôt Git pour votre projet WordPress et ajouter votre code source
-Configurer Jenkins pour surveiller le dépôt Git et récupérer automatiquement les mises à jour du code source
3-Configuration du pipeline de déploiement continu
-Créer un nouveau pipeline dans Jenkins et définir les étapes nécessaires pour le déploiement continu
-Ajouter une étape de construction pour installer et configurer WordPress en local
-Ajouter une étape de test pour exécuter des tests sur votre site WordPress
-Ajouter une étape de déploiement pour pousser les changements sur votre site WordPress en production
4-Configuration des notifications et des rapports
-Configurer des notifications par email pour les échecs de construction, de test ou de déploiement
-Intégrer des outils de qualité du code tels que SonarQube pour surveiller la qualité de votre code source
5-Exécution dupipeline de déploiement continu
-Lancer la pipeline de déploiement continu pour vérifier que tout fonctionne correctement
-Surveiller les résultats de chaque étape et apporter des corrections en cas de problème

## Installation du système:
### Linux : 
- Installer les différentes dépendances: virtualbox, libvirt, 
### Windows:
- Installer Cygwin (https://phoenixnap.com/kb/install-ansible-on-windows : method 1)
- Installer Vagrant pour windows (https://developer.hashicorp.com/vagrant/downloads)
- Installer VirtualBox pour windows (https://www.virtualbox.org/wiki/Downloads)
- Lancer Cygwin en administrateur, puis lancer le script host-deps.sh (utiliser sh devant)
- Lancer Vagrant en utilisant vagrant up, puis attendre 10-15 min pour la première installation 
**/!\ Sur Windows, si il y a une erreur de réseau qui indique le manque d'une carte réseau lors du lancement de vagrant, aller dans VirtualBox, cliquer sur "Outils" puis "Host-only Networks", et cliquer sur "Créer" en haut. **

## Fonctionnement du système:
Le fichier Vagrantfile installe tout ce qui est nécessaire pour notre utilisation. Il va dans un premier temps installer docker sur la machine virtuelle, puis il va lancer un premier playbook Ansible sur la machine virtuelle (playbook_git.yml) qui va installer mettre à jour la machine (apt upgrade), installer Git et Pip, télécharger le docker-compose du repo (ici) et installer les dépendances pour l'exécution du docker-compose (docker et docker-compose avec pip). Un deuxième playbook, est exécuté à la suite du premier, et permet de lancer le docker-compose ce qui va nous créer les différents services à travers des containers.
Les containers présents sont:

 - gitlab
 - wordpress
 - db
 - sonarqube
 - db_sonar
 - gitlab-runner

Une fois le lancement terminé, entrer << vagrant ssh >>.

## TODO
- [x] S'approprier Gitlab Runner
- [x] Automatiser la création des différents containeurs docker
- [x] Configurer avec ansible les différents services
- [x] Connecter le runner à Gitlab (manuel)
- [x] Connecter Sonarqube à Gitlab (manuel)
- [ ] Créer un projet "WordPress" et récupérer le Token Gitlab Runner de manière automatique
- [ ] Automatiser la connexion du runner à Gitlab (docker-compose? ansible?)
- [ ] Automatiser la connexion Sonarqube au runner
- [ ] Gérer la connexion WordPress <-> Gitlab
- [ ] Créer une pipeline qui lance sonarqube puis envoi automatiquement à WordPress (final)
