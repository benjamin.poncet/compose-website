#!/bin/bash

vagrant plugin update
vagrant plugin install vagrant-libvirt vagrant-docker-compose
ansible-galaxy collection install community.docker
ansible-galaxy collection install community.general
