#!/bin/bash

until curl -s -f http://localhost:80/ -o /dev/null; do
    echo "Waiting for gitlab to be up..."
    sleep 2
done

GITLAB_DOCKER_ID=$(docker ps | grep gitlab | grep -v runner | cut -f1 -d' ')
sleep 2
GITLAB_PASSWORD=$(sudo docker exec -it "${GITLAB_DOCKER_ID}" grep 'Password:' /etc/gitlab/initial_root_password | cut -f2 -d' ')

echo "Gilab docker ID: ${GITLAB_DOCKER_ID}"
echo "Gitlab root password: ${GITLAB_PASSWORD}"

echo "Generating Runner token"
TOKEN_COMMAND='gitlab-rails runner "print TokenAuthenticatableStrategies::EncryptionHelper.decrypt_token('"'"'$(echo '"'"'select runners_registration_token_encrypted from application_settings;'"'"' | gitlab-psql -d gitlabhq_production -qtAX)'"'"')"'
RUNNER_TOKEN="$(docker exec -it ${GITLAB_DOCKER_ID} /bin/bash -c "$TOKEN_COMMAND")"
echo "Runner token: ${RUNNER_TOKEN}"

curl --request POST --header "PRIVATE-TOKEN: ${RUNNER_TOKEN}" \
     --header "Content-Type: application/json" --data '{
        "name": "wordpress_pmn",
        "description": "Sample wordpress project", "path": "group",
        "namespace_id": "1", "initialize_with_readme": "false"}' \
     --url 'http://localhost/api/v4/projects/'
